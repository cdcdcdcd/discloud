import json
import subprocess
import sys
import os
import shutil
import zipfile
from upsettings import *

VERSION = 1
BROKEN = []

def exec_wait(cmdarg):
    result = subprocess.Popen(cmdarg, shell=True, stdout=subprocess.PIPE)
    result.wait()

def exec_nowait(cmdarg):
    result = subprocess.Popen(cmdarg, shell=True, stdout=subprocess.PIPE)
    return result

def decrypt_aes(file, password):
    global TARGETDIR
    """ encrypt file with aes-256-cbc with random password """
    inpath = os.path.join(TARGETDIR, file+'.enc')
    outpath = os.path.join(TARGETDIR, file)
    cmd = f'openssl aes-256-cbc -d -in "{inpath}" -out "{outpath}" -pass pass:{password}'
    print(cmd)
    result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    result.wait()

def cksum(file):
    """ checksum list (len 2) """
    cmd = f'cat "{file}" | cksum'
    print(cmd)
    result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    return result.stdout.read().decode('UTF-8').split()[0:2]

def dl_json(arg):
    """ extract json """
    global BROKEN, TARGETDIR
    json_file = open(arg, "r")
    data = json.load(json_file)
    json_file.close()
    files = []
    processes = []
    urls = data["urls"]
    if "channelid" in data.keys():
        print("new format")
        cid = data["channelid"]
    else:
        print("old format")
        cid = urls[0].split("/")[4] 
    for url in urls:
        file = url.split("/")[-1].split("?")[0] #ok
        files.append(file) #TODO working dir
    while len(urls) > 0:
        while len(urls) > 0:
            urlitem = urls.pop()
            urltrunk = "/".join(urlitem.split("/")[-2:])
            url = f"https://cdn.discordapp.com/attachments/{cid}/{urltrunk}"
            #TODO ...
            file = urltrunk.split("/")[-1].split("?")[0]
            cmd = f"wget -q \"{url}\" -O {file}"
            #cmd = f"curl -O {url}"
            print(cmd)
            processes.append([exec_nowait(cmd),url])
        for p in processes:
            p[0].wait()
            url = p[1]
            # file = url.split("/")[-1]
            # new_name = file.split("?")[0]
            if file in os.listdir("."):
                print(f"file {file} found")
            else:
                print(f"readding {file}")
                urls.append(url)

    filesstring = " ".join(files)
    # cat
    filename = data["filename"]
    if "password" in data.keys():
        filename = filename + ".enc"
    outpath = os.path.join(TARGETDIR, filename)
    cmd = f'cat {filesstring} > "{outpath}"'
    print(cmd)
    exec_wait(cmd)
    # decrypt
    encr = ""
    if "password" in data.keys():
        decrypt_aes(data["filename"], data["password"])
        encr = os.path.join(TARGETDIR,filename) 
    # clean
    for file in files:
        os.remove(file) #current folder

    if encr != "":
        os.remove(encr)
    if "cksum" in data.keys():
        file = os.path.join(TARGETDIR, data["filename"])
        if cksum(file) != data["cksum"]:
            print("#"*10+"\n\n"+"checksum not matching !!!"+"\n\n"+"#"*10)
            BROKEN.append(data["filename"])
        else:
            print("checksum OK")


def main():
    """ main """
    global BROKEN, TARGETDIR
    args = sys.argv[1:]
    for i,arg in enumerate(args):
        if arg.endswith("zip"):
            # shutil.rmtree(TMP)
            if not os.path.exists(TMP): os.mkdir(TMP)
            shutil.copy(arg, TMP)
            arg_file = arg.split("/")[-1]
            zip_path = os.path.join(TMP, arg_file)
            with zipfile.ZipFile(zip_path, 'r') as zip_ref:
                zip_ref.extractall(TMP)
            for j in os.listdir(TMP):
                if j.endswith("json"):
                    json_file = os.path.join(TMP,j)
                    dl_json(json_file)
                    shutil.remove(json_file)
        elif arg == "-d":
            TARGETDIR = args[i+1]
        elif arg.endswith("json"):
            dl_json(arg)

    if len(BROKEN) > 0:
        print("files with non matching checksum", BROKEN)
    for b in BROKEN:
        print(b)

main()
