import sys
import subprocess
import json
import random
import string
import time
from upsettings import *


VERSION = 2

def exec_wait(cmdarg):
    result = subprocess.Popen(cmdarg, shell=True, stdout=subprocess.PIPE)
    result.wait()

def randomword(length):
    """ random password """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))


def add_wdir(filename):
    """ add the working dir to filename """
    return WORKINGDIR + "/" + filename


def get_split_names():
    """ get the name """
    cmd = 'ls ' + WORKINGDIR + ' | grep "^x.*"'
    print(cmd)
    result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    filelist = result.stdout.read().decode('UTF-8').split()
    return list(map(add_wdir, filelist))


def encrypt_file(filename,password):
    chunk_size = M * 1000 * 1000  
    
    # Run openssl to encrypt the file and read its output
    proc = subprocess.Popen(["openssl", "aes-256-cbc", "-e", "-pass", "pass:" + password, "-in", filename], stdout=subprocess.PIPE)
    
    # Save each chunk to a separate file
    i = 0
    chunks = []
    while True:
        # Read a chunk from the output stream
        chunk = proc.stdout.read(chunk_size)
        if not chunk:
            break
    
        # Save the chunk to a file
        numstr = "{:05d}".format(i)
        chunkname = f"{WORKINGDIR}/x{numstr}"
        chunks.append(chunkname)
        with open(chunkname, "wb") as f:
            f.write(chunk)
        i += 1
    return chunks

def exec_wait_bulk(cmds):
    for cmd in cmds:
        exec_wait(cmd)

def abs_name(file):
    """ absolute name """
    return file.split("/")[-1] #TODO 

def cksum(file):
    """ checksum list (len 2) """
    cmd = f'cat "{file}"  | cksum'
    print(cmd)
    result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    result.wait()
    return result.stdout.read().decode('UTF-8').split()[0:2]

def encr_split(upfile):
    global OUTPATH
    print("outpath", OUTPATH)
    print("current file", upfile)
    basename = ".".join(upfile.split(".")[:-1])
    urls = []
    metadata = {}
    metadata["filename"] = abs_name(upfile)
    metadata["channelid"] = CID
    password = randomword(40)
    metadata["password"] = password
    metadata["cksum"] = cksum(upfile)
    print(metadata["cksum"])

    # check if splits with same cks as tosplit

    splits = get_split_names()
    print(splits)
    if len(splits) > 0:
        print("splits found, deleting")
        cmd = "rm " + WORKINGDIR + "/" + "x*"
        print(cmd)
        exec_wait(cmd)
    else:
        print("no splits found")
    splits = encrypt_file(upfile, password)

    # curl
    n = len(splits)
    file_batches = [splits[k:k+batch_size] for k in range(0, n, batch_size)]
    count_uploaded = 0
    for batch in file_batches:
        cmd = f'curl -H "Authorization: Bot {TOKEN}" -H "Content-Type: multipart/form-data" '
        for i, file in enumerate(batch):
            num = i+1
            cmd += f'-F "file{num}=@{file};type=image/png" '
        cmd += f'"https://discordapp.com/api/v9/channels/{CID}/messages"'
        print(cmd)
        success = False
        while not success:
            try:
                result = subprocess.Popen(cmd, shell=True, stdout =subprocess.PIPE) 
                result = result.stdout.read().decode('UTF-8')
                json_response = json.loads(result)
                for attachment in json_response['attachments']:
                    urls.append("/".join(attachment['url'].split("/")[5:]))
                success = True
                count_uploaded += len(batch)
                print("#"*10)
                print("progress ",int((count_uploaded/n)*100),"%")
                print("#"*10)
            except:
                print("fail")
                time.sleep(3)


    print("urls:", urls)
    metadata["urls"] = urls
    print("TIIIIIIIIIIIIIIIS", OUTPATH + "/" + abs_name(basename) + ".json")
    json_file = open(OUTPATH + "/" + abs_name(basename) + ".json", "w")
    print("json dump", metadata)
    json.dump(metadata, json_file)
    json_file.close()
    
    # clean
    cmds = [
            'rm ' + WORKINGDIR + '/x*',  # remove splits
            ]
    exec_wait_bulk(cmds)


def main():
    global OUTPATH
    if len(sys.argv[1:]) == 0:
        progname = sys.argv[0]
        print(f"{progname} VERSION {VERSION}")
        return
    print(sys.argv[1:])
    cmds = [
            "rm -r " + WORKINGDIR,
            "mkdir -p " + WORKINGDIR
            ]
    exec_wait_bulk(cmds)

    args = list(reversed(sys.argv[1:]))
    # for currentfile in sys.argv[1:]:
        # encr_split(currentfile)
    while(args):
        thisarg = args.pop()
        if (thisarg == '-o'):
            OUTPATH = args.pop()
            print("outpath", OUTPATH)
        else:
            encr_split(thisarg)
        
if __name__ == '__main__':
    main()
